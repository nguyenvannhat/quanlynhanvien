// vadilation sử lý tìm thấy và xuất ra lỗi của tên , mail , msnv ....

function Vadiladator() {
  // kiểm tra rỗng
  this.kiemTraRong = function (Taget, Error, messengerErr) {
    var valueTaget = document.getElementById(Taget).value.trim();
    if (valueTaget == "") {
      document.getElementById(Error).innerText = messengerErr;
      document.getElementById(Error).style.display = "block";
      return false;
    } else {
      document.getElementById(Error).innerText = "";
      document.getElementById(Error).style.display = "none";
      return true;
    }
  };
  // kiểm tra email
  this.kiemTraEmail = function (Taget, Error, messengerErr) {
    var valueTaget = document.getElementById(Taget).value.trim();
    var regex =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(valueTaget)) {
      document.getElementById(Error).innerText = messengerErr;
      document.getElementById(Error).style.display = "block";
      return false;
    } else {
      document.getElementById(Error).innerText = "";
      document.getElementById(Error).style.display = "none";
      return true;
    }
  };
  // kiểm tra tên
  this.HoTen = function (Taget, Error, messengerErr) {
    var valueTaget = document.getElementById(Taget).value.trim();
    var regex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
    if (!regex.test(valueTaget)) {
      document.getElementById(Error).innerText = messengerErr;
      document.getElementById(Error).style.display = "block";
      return false;
    } else {
      document.getElementById(Error).innerText = "";
      document.getElementById(Error).style.display = "none";
      return true;
    }
  };

  this.account = function (Taget, Error, messengerErr) {
    var valueTaget = document.getElementById(Taget).value.trim();
    var regex = /^([A-Za-z0-9]*)*$/;
    if (!regex.test(valueTaget)) {
      document.getElementById(Error).innerText = messengerErr;
      document.getElementById(Error).style.display = "block";
      return false;
    } else {
      document.getElementById(Error).innerText = "";
      document.getElementById(Error).style.display = "none";
      return true;
    }
  };
}
