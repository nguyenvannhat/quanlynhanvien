// khai báo và định nghĩa các lớp đối tượng

// định nghĩa các thuộc tính cho nhân viên
// 1 nhân viên bao gồm các thuộc tính sau :
var NhanVien = function (
  _taiKhoan,
  _hoTen,
  _email,
  _matKhau,
  _ngay,
  _salarry,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngay = _ngay;
  this.salarry = _salarry;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.totalSalarry = function () {
    let total = 0;
    if (_chucVu === "sep") {
      total = _salarry * 3;
    } else if (_chucVu === "truong phong") {
      total = _salarry * 2;
    } else {
      total = _salarry;
    }
    return total;
  };
  this.xepLoai = function () {
    let loai = "";
    if (this.gioLam >= 192) {
      loai = "Nhân viên xuất sắc";
    } else if (this.gioLam >= 172) {
      loai = "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      loai = "Nhân viên khá";
    } else {
      loai = "Nhân viên trung bình";
    }
    return loai;
  };
};
