// lấy thông tin từ form đăng nhập
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngay = document.getElementById("datepicker").value;
  var salarry = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  var nhanVien = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngay,
    salarry,
    chucVu,
    gioLam
  );

  return nhanVien;
}

// xuất danh sách nhân viên đã nhập
function xuatDanhSachNhanVien(dsnv) {
  // biến để chứa các thẻ TR
  var contentHTML = "";
  for (var index = 0; index < dsnv.length; index++) {
    var nhanVien = dsnv[index];

    // chấm đến các thuộc tính trong nhanvienmodel
    var contenttr = `<tr>
  <td>${nhanVien.taiKhoan}</td>
  <td>${nhanVien.hoTen}</td>
  <td>${nhanVien.email}</td>
  <td>${nhanVien.matKhau}</td>
  <td>${nhanVien.ngay}</td>
  <td>${nhanVien.totalSalarry()}</td>
  <td>${nhanVien.xepLoai()}</td>
  <td>
        
        
         <button class="btn btn-info" onclick="xoaNhanVien('${
           nhanVien.taiKhoan
         }')">Xóa</button>
         <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="suaNhanVien('${
           nhanVien.taiKhoan
         }')">Sửa</button>
      
        
  </td>
  
  </tr>`;

    contentHTML += contenttr;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// in thông tin nhân viên đã nhập lên form
function xuatThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngay;
  document.getElementById("luongCB").value = nv.salarry;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}

/*  <button class="btn btn-success" onclick="suaNhanVien('${
           nhanVien.taiKhoan
         }')">Sửa</button>  */
