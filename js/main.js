// mảng để hứng danh sách các nhân viên
var danhSachNhanVien = [];

const DSNV_LOCALSTORAGE = " dsNhanVien ";

// tìm kiếm vị trí để xóa
const timKiemViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
};

// tạo biến mới để hứng validator
var vadilationNV = new Vadiladator();

// hàm lưu localstorage
const luuLocalStorge = function () {
  var dsnv = JSON.stringify(danhSachNhanVien);

  localStorage.setItem(DSNV_LOCALSTORAGE, dsnv);

  xuatDanhSachNhanVien(danhSachNhanVien);
};

// lấy dữ liệu khi user tải lại trang
var dsnv = localStorage.getItem(DSNV_LOCALSTORAGE);
// gán lại array gốc vả render
if (dsnv) {
  danhSachNhanVien = JSON.parse(dsnv);
  danhSachNhanVien = danhSachNhanVien.map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngay,
      item.salarry,
      item.chucVu,
      item.gioLam
    );
  });
  xuatDanhSachNhanVien(danhSachNhanVien);
}

// hàm thêm nhân viên
function themNhanVien() {
  // tạo biến để hứng nhân viên mới
  var nhanVienMoi = layThongTinTuForm();

  // kiểm tra input
  if (
    !vadilationNV.kiemTraRong(
      "email",
      "tbEmail",
      " email không được để trống "
    ) ||
    !vadilationNV.kiemTraEmail(
      "email",
      "tbEmail",
      "địa chỉ email không hợp lệ : => @ và .com "
    ) ||
    !vadilationNV.kiemTraRong("name", "tbTen", "Tên không được để trống ") ||
    !vadilationNV.HoTen("name", "tbTen", "Tên không hợp lệ") ||
    !vadilationNV.kiemTraRong(
      "tknv",
      "tbTKNV",
      " Tài khoảng đăng nhập không được để trống "
    ) ||
    !vadilationNV.account("tknv", "tbTKNV", "Tài khoản không hợp lệ") ||
    !vadilationNV.kiemTraRong(
      "password",
      "tbMatKhau",
      "Tên không được để trống "
    ) ||
    !vadilationNV.kiemTraRong(
      "datepicker",
      "tbNgay",
      "Ngày Tháng Năm không được để trống "
    ) ||
    !vadilationNV.kiemTraRong(
      "luongCB",
      "tbLuongCB",
      "Lương cơ bản không được để trống  "
    ) ||
    !vadilationNV.kiemTraRong(
      "chucvu",
      "tbChucVu",
      "Chức vụ không được để trống  "
    ) ||
    !vadilationNV.kiemTraRong(
      "gioLam",
      "tbGiolam",
      "Giờ làm không được để trống "
    )
  ) {
    return;
  }
  // push nhân viên mới vào mảng
  danhSachNhanVien.push(nhanVienMoi);
  // xuất danh sách nhân viên
  xuatDanhSachNhanVien(danhSachNhanVien);
  // lưu local
  luuLocalStorge();
}

// hàm xóa nhân viên
function xoaNhanVien(id) {
  var viTri = timKiemViTri(id, danhSachNhanVien);
  danhSachNhanVien.splice(viTri, 1);
  luuLocalStorge();
}

// sửa nhân viên
function suaNhanVien(id) {
  var edit = timKiemViTri(id, danhSachNhanVien);
  var nhanVien = danhSachNhanVien[edit];
  xuatThongTinLenForm(nhanVien);
  luuLocalStorge();
}
// cập nhật nhân viên
function capNhatNhanVien() {
  var capNhatNhanVien = layThongTinTuForm();
  let viTri = timKiemViTri(capNhatNhanVien.taiKhoan, danhSachNhanVien);
  danhSachNhanVien[viTri] = capNhatNhanVien;
  xuatDanhSachNhanVien(capNhatNhanVien);
  luuLocalStorge();
  console.log(danhSachNhanVien);
}

// tìm kiếm nhân viên
function timKiemNhanVien() {
  var txtSearh = document.getElementById("searchName").value;
  if (txtSearh == "Nhân viên xuất sắc") {
    return (document.getElementById("btnTimNV").innerHTML =
      xuatDanhSachNhanVien(danhSachNhanVien));
  }

  // console.log(xuatDanhSachNhanVien(danhSachNhanVien));
  luuLocalStorge();
}
